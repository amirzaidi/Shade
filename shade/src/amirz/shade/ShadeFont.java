package amirz.shade;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.text.TextUtils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class ShadeFont {
    //public static final String KEY_FONT = "pref_font";
    public static final String DEFAULT_FONT = "google_sans";

    private static Map<String, Typeface> sDeviceMap;

    @SuppressWarnings("JavaReflectionMemberAccess")
    @SuppressLint("InflateParams")
    public static void override(Context context) {
        String font = getFont(context);

        try {
            final Field staticField = Typeface.class.getDeclaredField("sSystemFontMap");
            staticField.setAccessible(true);
            if (sDeviceMap == null) {
                //noinspection unchecked
                sDeviceMap = (Map<String, Typeface>) staticField.get(null);
            }

            Map<String, Typeface> newMap = new HashMap<>(sDeviceMap);

            Typeface regular, medium, bold;
            if (TextUtils.isEmpty(font)) {
                regular = sDeviceMap.get("sans-serif");
                medium = sDeviceMap.get("sans-serif-medium");
                bold = sDeviceMap.get("sans-serif-bold");
            } else {
                AssetManager assets = context.getAssets();
                regular = Typeface.createFromAsset(assets, font + "_regular.ttf");
                medium = Typeface.createFromAsset(assets, font + "_medium.ttf");
                bold = Typeface.createFromAsset(assets, font + "_bold.ttf");
            }

            newMap.put("custom-sans-serif", regular);
            newMap.put("custom-sans-serif-medium", medium);
            newMap.put("custom-sans-serif-bold", bold);
            staticField.set(null, newMap);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static String getFont(Context context) {
        return DEFAULT_FONT;
        //return Utilities.getPrefs(context).getString(KEY_FONT, DEFAULT_FONT);
    }
}
