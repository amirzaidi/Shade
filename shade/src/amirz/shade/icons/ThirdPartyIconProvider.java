package amirz.shade.icons;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;

import com.android.launcher3.InvariantDeviceProfile;
import com.android.launcher3.util.ComponentKey;

import amirz.shade.icons.pack.IconResolver;

@SuppressWarnings("unused")
public class ThirdPartyIconProvider extends RoundIconProvider {
    private final Context mContext;
    private final int mIconDpi;

    public ThirdPartyIconProvider(Context context) {
        super(context);
        mContext = context;
        mIconDpi = InvariantDeviceProfile.INSTANCE.get(context).fillResIconDpi;
    }

    @SuppressLint("WrongConstant")
    @Override
    public Drawable getIcon(ComponentKey key) {
        IconResolver.DefaultDrawableProvider fallback = () -> super.getIcon(key);
        Drawable icon = ThirdPartyIconUtils.getByKey(mContext, key, mIconDpi, fallback);
        if (icon == null) {
            icon = fallback.get();
        }
        return icon;
    }
}
