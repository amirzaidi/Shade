package amirz.shade;

import android.os.Bundle;

import com.android.launcher3.uioverrides.QuickstepLauncher;

import amirz.shade.util.AppReloader;
import amirz.unread.UnreadSession;

public class ShadeLauncher extends QuickstepLauncher {
    private UnreadSession mUnread;

    public ShadeLauncher() {
        new ShadeLauncherCallbacks(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ShadeFont.override(this);
        super.onCreate(savedInstanceState);
        mUnread = UnreadSession.getInstance(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mUnread.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mUnread.onPause(this);
    }
}
