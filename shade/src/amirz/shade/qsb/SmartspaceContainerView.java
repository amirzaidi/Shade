package amirz.shade.qsb;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProviderInfo;
import android.content.ContentUris;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Process;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.launcher3.Launcher;
import com.android.launcher3.R;
import com.android.launcher3.qsb.QsbContainerView;

import static android.appwidget.AppWidgetManager.ACTION_APPWIDGET_BIND;
import static android.appwidget.AppWidgetManager.EXTRA_APPWIDGET_ID;
import static android.appwidget.AppWidgetManager.EXTRA_APPWIDGET_PROVIDER;

public class SmartspaceContainerView {
    static final String WIDGET_CLASS_NAME = "com.google.android.apps.gsa"
            + ".staticplugins.smartspace.widget.SmartspaceWidgetProvider";
    static final String WIDGET_PACKAGE_NAME = "com.google.android.googlequicksearchbox";

    public static class SmartspaceFragment extends QsbContainerView.QsbFragment {
        private static final int SMART_SPACE_WIDGET_HOST_ID = 1027;
        private static final int REQUEST_BIND_QSB = 1;

        private QsbContainerView.QsbWidgetHost mQsbWidgetHost;

        public SmartspaceFragment() {
            mKeyWidgetId = "smart_space_widget_id";
        }

        @Override
        protected QsbContainerView.QsbWidgetHost createHost() {
            mQsbWidgetHost = new QsbContainerView.QsbWidgetHost(getContext(),
                    SMART_SPACE_WIDGET_HOST_ID,
                    ThemedSmartspaceHostView::new);
            return mQsbWidgetHost;
        }

        @Override
        protected AppWidgetProviderInfo getSearchWidgetProvider() {
            for (AppWidgetProviderInfo providerInfo :
                    AppWidgetManager.getInstance(getContext()).getInstalledProviders()) {
                if (providerInfo.getProfile().equals(Process.myUserHandle())
                        && WIDGET_PACKAGE_NAME.equals(providerInfo.provider.getPackageName())
                        && WIDGET_CLASS_NAME.equals(providerInfo.provider.getClassName())) {
                    return providerInfo;
                }
            }
            return null;
        }

        @Override
        protected Bundle createBindOptions() {
            Bundle createBindOptions = super.createBindOptions();
            createBindOptions.putString("attached-launcher-identifier", getContext().getPackageName());
            createBindOptions.putBoolean("com.google.android.apps.gsa.widget.PREINSTALLED", true);
            return createBindOptions;
        }

        @Override
        protected View getDefaultView(ViewGroup container, boolean showSetupIcon) {
            View v = getDateView(container);

            if (showSetupIcon) {
                Intent intent = new Intent(ACTION_APPWIDGET_BIND)
                        .putExtra(EXTRA_APPWIDGET_ID, mQsbWidgetHost.allocateAppWidgetId())
                        .putExtra(EXTRA_APPWIDGET_PROVIDER, getSearchWidgetProvider().provider);

                v.setOnClickListener(v2 -> startActivityForResult(intent, REQUEST_BIND_QSB));

                // Try binding immediately when creating the view.
                v.callOnClick();
            } else {
                v.setOnClickListener(this::openCalendar);
            }

            return v;
        }

        private void openCalendar(View v) {
            Uri.Builder timeUri = CalendarContract.CONTENT_URI.buildUpon().appendPath("time");
            ContentUris.appendId(timeUri, System.currentTimeMillis());
            Intent intent = new Intent(Intent.ACTION_VIEW)
                    .setData(timeUri.build())
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
            Launcher.getLauncher(getContext())
                    .startActivitySafely(v, intent, null, null);
        }
    }

    public static View getDateView(ViewGroup root) {
        return LayoutInflater.from(root.getContext())
                .inflate(R.layout.smart_space_date_view, root, false);
    }
}
