package amirz.shade;

import android.os.Bundle;

import com.android.launcher3.LauncherCallbacks;

import java.io.FileDescriptor;
import java.io.PrintWriter;

public class ShadeLauncherCallbacks implements LauncherCallbacks {
    private final ShadeLauncher mLauncher;

    public ShadeLauncherCallbacks(ShadeLauncher launcher) {
        mLauncher = launcher;
        launcher.setLauncherCallbacks(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
    }

    @Override
    public void dump(String prefix, FileDescriptor fd, PrintWriter w, String[] args) {
    }

    @Override
    public void onHomeIntent(boolean internalStateHandled) {
    }

    @Override
    public boolean startSearch(String initialQuery, boolean selectInitialQuery,
                               Bundle appSearchData) {
        return false;
    }
}
