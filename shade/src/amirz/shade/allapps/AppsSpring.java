package amirz.shade.allapps;

import android.graphics.Canvas;
import android.widget.EdgeEffect;

import androidx.dynamicanimation.animation.FloatPropertyCompat;
import androidx.dynamicanimation.animation.SpringAnimation;
import androidx.dynamicanimation.animation.SpringForce;

import static androidx.dynamicanimation.animation.SpringForce.DAMPING_RATIO_MEDIUM_BOUNCY;
import static androidx.dynamicanimation.animation.SpringForce.STIFFNESS_LOW;
import static androidx.dynamicanimation.animation.SpringForce.STIFFNESS_MEDIUM;

class AppsSpring {
    private static final float STIFFNESS = (STIFFNESS_MEDIUM + STIFFNESS_LOW) / 2;
    private static final float DAMPING_RATIO = DAMPING_RATIO_MEDIUM_BOUNCY;

    private static final float ABSORB_SCALE = 0.3f;
    private static final float DRAG_SCALE = 0.1f;

    private static final FloatPropertyCompat<AppsSpring> DAMPED_SCROLL =
            new FloatPropertyCompat<AppsSpring>("value") {
                @Override
                public float getValue(AppsSpring object) {
                    return object.mDisplacement;
                }

                @Override
                public void setValue(AppsSpring object, float value) {
                    object.mDisplacement = value;
                    object.updateDampedScrollShift();
                }
            };

    private final AppsContainerView mView;
    private final SpringAnimation mSpring;
    private float mDisplacement;

    AppsSpring(AppsContainerView view) {
        mView = view;

        mSpring = new SpringAnimation(this, DAMPED_SCROLL, 0);
        mSpring.setSpring(new SpringForce(0)
                .setStiffness(STIFFNESS)
                .setDampingRatio(DAMPING_RATIO));
    }

    private void updateDampedScrollShift() {
        mView.setDampedScrollShift(mDisplacement);
    }

    private void finishScrollWithVelocity(float velocity) {
        mDisplacement = mView.getDampedScrollShift();
        mSpring.setStartVelocity(velocity);
        mSpring.setStartValue(mDisplacement);
        mSpring.start();
    }

    EdgeEffect createSide(float multiplier) {
        return new EdgeEffect(mView.getContext()) {
            private boolean mFinished = true;

            @Override
            public boolean isFinished() {
                return mFinished;
            }

            @Override
            public boolean draw(Canvas canvas) {
                return false;
            }

            @Override
            public void onPull(float deltaDistance) {
                onPull(deltaDistance, 0.5f);
            }

            @Override
            public void onPull(float deltaDistance, float displacement) {
                mDisplacement += multiplier * deltaDistance * mView.getHeight() * DRAG_SCALE;
                updateDampedScrollShift();
                mFinished = false;
            }

            @Override
            public void onAbsorb(int velocity) {
                finishScrollWithVelocity(multiplier * velocity * ABSORB_SCALE);
                mFinished = false;
            }

            @Override
            public void onRelease() {
                finishScrollWithVelocity(0f);
                mFinished = true;
            }
        };
    }
}
