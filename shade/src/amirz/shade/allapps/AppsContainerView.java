package amirz.shade.allapps;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EdgeEffect;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.launcher3.Utilities;
import com.android.launcher3.allapps.LauncherAllAppsContainerView;

public class AppsContainerView extends LauncherAllAppsContainerView {
    private final AppsSpring mController;
    private float mDampedScrollShift;

    public AppsContainerView(Context context) {
        this(context, null);
    }

    public AppsContainerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AppsContainerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mController = new AppsSpring(this);
    }

    @Override
    public RecyclerView.EdgeEffectFactory createEdgeEffectFactory() {
        return new RecyclerView.EdgeEffectFactory() {
            @NonNull
            @Override
            protected EdgeEffect createEdgeEffect(@NonNull RecyclerView view, int direction) {
                switch (direction) {
                    case DIRECTION_TOP:
                        return mController.createSide(+1f);
                    case DIRECTION_BOTTOM:
                        return mController.createSide(-1f);
                }
                return super.createEdgeEffect(view, direction);
            }
        };
    }

    public float getDampedScrollShift() {
        return mDampedScrollShift;
    }

    @Override
    public void setDampedScrollShift(float shift) {
        float maxShift = getSearchView().getHeight() / 2f;
        mDampedScrollShift = Utilities.boundToRange(shift, -maxShift, maxShift);
        super.setDampedScrollShift(mDampedScrollShift);
    }
}
