package amirz.shade.util;

import android.content.Context;
import android.content.pm.LauncherActivityInfo;
import android.content.pm.LauncherApps;
import android.os.UserHandle;

import com.android.launcher3.LauncherAppState;
import com.android.launcher3.LauncherModel;
import com.android.launcher3.model.data.ItemInfo;
import com.android.launcher3.pm.UserCache;
import com.android.launcher3.util.ComponentKey;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import amirz.shade.customization.IconDatabase;

public class AppReloader {
    private static AppReloader sInstance;

    public static synchronized AppReloader get(Context context) {
        if (sInstance == null) {
            sInstance = new AppReloader(context);
        }
        return sInstance;
    }

    private final Context mContext;
    private final LauncherModel mModel;
    private final UserCache mUsers;
    private final LauncherApps mApps;

    private AppReloader(Context context) {
        mContext = context;
        mModel = LauncherAppState.getInstance(context).getModel();
        mUsers = UserCache.INSTANCE.get(mContext);
        mApps = mContext.getSystemService(LauncherApps.class);
    }

    public Set<ComponentKey> withIconPack(String iconPack) {
        return matchFilter(key -> IconDatabase.getByComponent(mContext, key).equals(iconPack));
    }

    private Set<ComponentKey> matchFilter(ReloadFilter filter) {
        Set<ComponentKey> reloadKeys = new HashSet<>();
        for (UserHandle user : mUsers.getUserProfiles()) {
            for (LauncherActivityInfo info : mApps.getActivityList(null, user)) {
                ComponentKey key = new ComponentKey(info.getComponentName(), info.getUser());
                if (filter.shouldReload(key)) {
                    reloadKeys.add(key);
                }
            }
        }
        return reloadKeys;
    }

    public void reload() {
        reload(matchFilter(componentKey -> true));
    }

    public void reload(ItemInfo item) {
        reload(item.getTargetComponent().getPackageName(), item.user);
    }

    public void reload(ComponentKey key) {
        reload(key.componentName.getPackageName(), key.user);
    }

    public void reload(Collection<ComponentKey> keys) {
        for (ComponentKey key : keys) {
            reload(key);
        }
    }

    private void reload(String pkg, UserHandle user) {
        mModel.onAppIconChanged(pkg, user);
    }

    private interface ReloadFilter {
        boolean shouldReload(ComponentKey key);
    }
}
