/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.launcher3.uioverrides;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.util.Pair;

import com.android.launcher3.uioverrides.dynamicui.ColorExtractionAlgorithm;
import com.android.launcher3.uioverrides.dynamicui.WallpaperColorsCompat;
import com.android.launcher3.uioverrides.dynamicui.WallpaperManagerCompat;
import com.android.launcher3.util.MainThreadInitializedObject;
import com.android.systemui.shared.system.TonalCompat.ExtractionInfo;

import java.util.ArrayList;

import static android.app.WallpaperManager.FLAG_SYSTEM;

@TargetApi(Build.VERSION_CODES.P)
public class WallpaperColorInfo implements WallpaperManagerCompat.OnColorsChangedListenerCompat {

    private static final int MAIN_COLOR_LIGHT = 0xffdadce0;
    private static final int MAIN_COLOR_DARK = 0xff202124;
    private static final int MAIN_COLOR_REGULAR = 0xff000000;

    private static final int FALLBACK_COLOR = Color.WHITE;

    public static final MainThreadInitializedObject<WallpaperColorInfo> INSTANCE =
            new MainThreadInitializedObject<>(WallpaperColorInfo::new);

    private final ArrayList<OnChangeListener> mListeners = new ArrayList<>();
    private final WallpaperManagerCompat mWallpaperManager;
    private final ColorExtractionAlgorithm mExtractionType;
    private final ExtractionInfo mExtractionInfo;

    private OnChangeListener[] mTempListeners = new OnChangeListener[0];

    private WallpaperColorInfo(Context context) {
        mWallpaperManager = WallpaperManagerCompat.getInstance(context);
        mExtractionType = new ColorExtractionAlgorithm();
        mExtractionInfo = new ExtractionInfo();
        mWallpaperManager.addOnColorsChangedListener(this);
        update(mWallpaperManager.getWallpaperColors(FLAG_SYSTEM));
    }

    public int getMainColor() {
        return mExtractionInfo.mainColor;
    }

    public int getSecondaryColor() {
        return mExtractionInfo.secondaryColor;
    }

    public boolean isDark() {
        return mExtractionInfo.supportsDarkTheme;
    }

    public boolean supportsDarkText() {
        return mExtractionInfo.supportsDarkText;
    }

    public boolean isMainColorDark() {
        return mExtractionInfo.mainColor == MAIN_COLOR_DARK;
    }

    @Override
    public void onColorsChanged(WallpaperColorsCompat colors, int which) {
        if ((which & FLAG_SYSTEM) != 0) {
            update(colors);
            notifyChange();
        }
    }

    private void update(WallpaperColorsCompat wallpaperColors) {

        Pair<Integer, Integer> colors = mExtractionType.extractInto(wallpaperColors);
        if (colors != null) {
            mExtractionInfo.mainColor = colors.first;
            mExtractionInfo.secondaryColor = colors.second;
        } else {
            mExtractionInfo.mainColor = FALLBACK_COLOR;
            mExtractionInfo.secondaryColor = FALLBACK_COLOR;
        }
        mExtractionInfo.supportsDarkText = wallpaperColors != null
                ? (wallpaperColors.getColorHints()
                & WallpaperColorsCompat.HINT_SUPPORTS_DARK_TEXT) > 0 : false;
        mExtractionInfo.supportsDarkTheme = wallpaperColors != null
                ? (wallpaperColors.getColorHints()
                & WallpaperColorsCompat.HINT_SUPPORTS_DARK_THEME) > 0 : false;
    }

    public void addOnChangeListener(OnChangeListener listener) {
        mListeners.add(listener);
    }

    public void removeOnChangeListener(OnChangeListener listener) {
        mListeners.remove(listener);
    }

    private void notifyChange() {
        // Create a new array to avoid concurrent modification when the activity destroys itself.
        mTempListeners = mListeners.toArray(mTempListeners);
        for (OnChangeListener listener : mTempListeners) {
            if (listener != null) {
                listener.onExtractedColorsChanged(this);
            }
        }
    }

    public interface OnChangeListener {
        void onExtractedColorsChanged(WallpaperColorInfo wallpaperColorInfo);
    }
}
