package amirz.shade.qsb;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import com.android.launcher3.views.DoubleShadowBubbleTextView;

public class DoubleShadowTextView extends AppCompatTextView {
    public final Paint mPaint;
    public DoubleShadowBubbleTextView.ShadowInfo mShadowInfo;

    public DoubleShadowTextView(Context context) {
        this(context, null);
    }

    public DoubleShadowTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DoubleShadowTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG);
        mShadowInfo = new DoubleShadowBubbleTextView.ShadowInfo(context, attrs, defStyleAttr);
        initDefaultShadowLayer();
    }

    private void initDefaultShadowLayer() {
        setShadowLayer(Math.max(mShadowInfo.keyShadowBlur + mShadowInfo.keyShadowOffset,
                mShadowInfo.ambientShadowBlur), 0f, 0f, mShadowInfo.keyShadowColor);
    }

    public void onDraw(Canvas canvas) {
        if (mShadowInfo.skipDoubleShadow(this)) {
            super.onDraw(canvas);
            return;
        }

        getPaint().setShadowLayer(mShadowInfo.ambientShadowBlur, 0f, 0f,
                mShadowInfo.ambientShadowColor);
        super.onDraw(canvas);

        getPaint().setShadowLayer(mShadowInfo.keyShadowBlur, 0f, mShadowInfo.keyShadowOffset,
                mShadowInfo.keyShadowColor);
        super.onDraw(canvas);
    }
}
