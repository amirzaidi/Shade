package amirz.unread.notifications;

import android.app.Notification;
import android.app.PendingIntent;
import android.service.notification.StatusBarNotification;

import com.android.launcher3.util.PackageUserKey;

public class ParsedNotification {
    private static final String[] EMPTY_TITLE = new String[0];
    private static final String EMPTY_TEXT = "";

    public final PendingIntent pi;
    public final String pkg;
    public final String[] splitTitle;
    public final String text;

    public ParsedNotification(StatusBarNotification sbn) {
        PackageUserKey packageUserKey = PackageUserKey.fromNotification(sbn);
        Notification notification = sbn.getNotification();
        CharSequence titleCs = notification.extras.getCharSequence(Notification.EXTRA_TITLE);
        CharSequence textCs = notification.extras.getCharSequence(Notification.EXTRA_TEXT);

        pi = notification.contentIntent;
        pkg = packageUserKey.mPackageName;
        splitTitle = titleCs == null
                ? EMPTY_TITLE
                : splitTitle(titleCs.toString());
        text = textCs == null
                ? EMPTY_TEXT
                : textCs.toString().trim().split("\n")[0]; // First line
    }

    private String[] splitTitle(String title) {
        final String[] delimiters = {": ", " - ", " • "};
        for (String del : delimiters) {
            if (title.contains(del)) {
                return title.split(del, 2);
            }
        }
        return new String[] { title };
    }
}
