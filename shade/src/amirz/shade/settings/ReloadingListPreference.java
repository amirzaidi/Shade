package amirz.shade.settings;

import android.content.Context;
import android.util.AttributeSet;

import androidx.preference.ListPreference;

import java.util.function.Function;

import static com.android.launcher3.util.Executors.MAIN_EXECUTOR;
import static com.android.launcher3.util.Executors.THREAD_POOL_EXECUTOR;

@SuppressWarnings("unused")
public class ReloadingListPreference extends ListPreference {
    private Function<ReloadingListPreference, Runnable> mOnReloadListener;

    public ReloadingListPreference(Context context) {
        super(context);
    }

    public ReloadingListPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ReloadingListPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ReloadingListPreference(Context context, AttributeSet attrs, int defStyleAttr,
                                   int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setDataLoader(Function<ReloadingListPreference, Runnable> onReloadListener) {
        mOnReloadListener = onReloadListener;
    }

    @Override
    protected void onClick() {
        // Run the entries updater on the main thread immediately.
        // Should be fast as the data was cached from the async load before.
        // If it wasn't, we need to block to ensure the data has been loaded.
        //loadEntries(false);
        super.onClick();
    }

    public void loadEntries() {
        loadEntries(true);
    }

    private void loadEntries(boolean async) {
        if (async) {
            THREAD_POOL_EXECUTOR.execute(
                    () -> MAIN_EXECUTOR.execute(mOnReloadListener.apply(this)));
        } else {
            mOnReloadListener.apply(this).run();
        }
    }

    public void setEntriesWithValues(CharSequence[] entries, CharSequence[] entryValues) {
        setEntries(entries);
        setEntryValues(entryValues);
        setSummary("%s");
    }
}
