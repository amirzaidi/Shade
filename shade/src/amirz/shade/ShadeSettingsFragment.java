package amirz.shade;

import android.content.Context;

import androidx.preference.Preference;
import androidx.preference.PreferenceScreen;

import com.android.launcher3.settings.SettingsActivity;

import amirz.shade.customization.IconDatabase;
import amirz.shade.settings.IconPackPrefSetter;
import amirz.shade.settings.ReloadingListPreference;
import amirz.shade.util.AppReloader;

@SuppressWarnings("unused")
public class ShadeSettingsFragment extends SettingsActivity.LauncherSettingsFragment {
    private static final String KEY_ICON_PACK = "pref_icon_pack";

    @Override
    protected boolean initPreference(Preference preference) {
        final Context context = getActivity();
        switch (preference.getKey()) {
            case KEY_ICON_PACK:
                ReloadingListPreference icons = (ReloadingListPreference) preference;
                icons.setValue(IconDatabase.getGlobal(context));
                icons.setDataLoader(new IconPackPrefSetter(context));
                icons.setOnPreferenceChangeListener((pref, val) -> {
                    IconDatabase.clearAll(context);
                    IconDatabase.setGlobal(context, (String) val);
                    AppReloader.get(context).reload();
                    return true;
                });
                return true;
        }
        return super.initPreference(preference);
    }

    @Override
    public void onResume() {
        super.onResume();
        PreferenceScreen screen = getPreferenceScreen();
        for (int i = 0; i < screen.getPreferenceCount(); i++) {
            Preference preference = screen.getPreference(i);
            if (preference instanceof ReloadingListPreference) {
                ((ReloadingListPreference) preference).loadEntries();
            }
        }
    }
}
